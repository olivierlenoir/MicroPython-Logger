"""
Author: Olivier Lenoir <olivier.len02@gmail.com>
Created: 2022-06-22 18:28:00
License: MIT, Copyright (c) 2021-2024 Olivier Lenoir
Language: MicroPython v1.22.2
Project: MicroPython boot file
Description:
Repository: https://gitlab.com/olivierlenoir/MicroPython-Logger
"""


from json import dumps
from logger import Logger

_log = Logger()


# SoftAPWebServer
# To activate uncomment here bellow lines and replace '<Password>'
"""
from softapwebserver import SoftAPWebServer


def http_rsp():
    rsp = [
        'HTTP/1.1 200 OK',
        f'Content-Length: {len(dumps(_log.data))}',
        'Content-Type: application/json; charset=utf-8',
        '',
        f'{dumps(_log.data)}'
    ]
    return '\r\n'.join(rsp).encoding('utf-8')


_apws = SoftAPWebServer(
    essid=_log.object_id,
    password='<Password>',
    security=3,
    http_msg=http_rsp
    )
"""
