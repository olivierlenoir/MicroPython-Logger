"""
Author: Olivier Lenoir <olivier.len02@gmail.com>
Created: 2021-03-30 18:36:43
License: MIT, Copyright (c) 2021-2024 Olivier Lenoir
Language: MicroPython V1.22.2
Project: Logger
Description: power on counter and times 10 minutes counter
Repository: https://gitlab.com/olivierlenoir/MicroPython-Logger
"""


from json import load, dump
from machine import unique_id, Timer
from binascii import hexlify
from micropython import alloc_emergency_exception_buf, schedule


alloc_emergency_exception_buf(100)


class Logger:
    """Logger"""

    def __init__(self):
        self.log_file = 'logger_{}.json'.format(self._unique_id())
        self._init()

    def _init(self):
        """Load or initialize log file"""
        self._tim10 = Timer(-1)  # Init virtual timer
        try:
            self.data = self.load()
        except OSError:
            self.data = {
                'unique_id': self._unique_id(),
                'object_id': self._unique_id(),
                'power_on': 0,  # power on or reset device counter
                'on_time_10': 0,  # 10 minutes on power counter
                'on_time_histogram': {'0': 0},  # 10 minutes on power histogram
                }
            self.dump()
        self._power_on()
        self.start()

    def load(self):
        """Read data from json log file"""
        with open(self.log_file) as logfile:
            return load(logfile)

    def dump(self):
        """Write data in json log file"""
        with open(self.log_file, 'w') as logfile:
            dump(self.data, logfile)

    @staticmethod
    def _unique_id():
        """Return unique_id of the device"""
        return hexlify(unique_id()).decode('utf-8')

    @property
    def object_id(self):
        """Return object_id"""
        return self.data['object_id']

    @object_id.setter
    def object_id(self, object_id):
        """Set object_id"""
        self.data['object_id'] = object_id
        self.dump()

    def _power_on_time_histogram(self):
        """Updade on_time_histogram"""
        cur_hist = str(self.data['on_time_histogram']['0'])
        if cur_hist != '0':
            if cur_hist in self.data['on_time_histogram']:
                self.data['on_time_histogram'][cur_hist] += 1
            else:
                self.data['on_time_histogram'][cur_hist] = 1
            self.data['on_time_histogram']['0'] = 0

    def _power_on(self):
        """Increment power_on counter, updade on_time_histogram"""
        self.data['power_on'] += 1
        self._power_on_time_histogram()
        self.dump()

    def _init_timer10(self):
        """Periodic 10 minutes timer"""
        self._tim10.init(
            period=600_000,  # 10 minutes or 600000 ms
            mode=Timer.PERIODIC,
            callback=lambda t: schedule(self._on_time_10, t)
            )

    def _on_time_10(self, _):
        """Increment on time every 10 minutes, update on_time_histogram['0']"""
        self.data['on_time_10'] += 1
        self.data['on_time_histogram']['0'] += 1
        self.dump()

    def start(self):
        """Init timers"""
        self._init_timer10()

    def stop(self):
        """Deinit timers"""
        self._tim10.deinit()
