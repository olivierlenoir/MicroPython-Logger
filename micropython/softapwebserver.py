"""
Author: Olivier Lenoir <olivier.len02@gmail.com>
Created: 2023-07-19 17:54:27
License: MIT, Copyright (c) 2023-2024 Olivier Lenoir
Language: MicroPython v1.22.2 using ESP32
Project: Soft Access Point Web Server
Description:
Repository: https://gitlab.com/olivierlenoir/MicroPython-Logger
"""


import network
import socket
import _thread
from time import sleep_ms


MAX_CONNECTION = const(5)


class SoftAPWebServer:
    """Soft Access Point Web Server"""

    def __init__(self, essid, password, security=3, http_msg='HTTP/1.1 200 OK\r\n'):
        self.essid = essid
        self.password = password
        self.security = security
        self.http_msg = http_msg
        self.debug = 0
        self._init_sap()
        self._init_socket()
        self._init_web_server()

    def _init_sap(self):
        """Init Soft Access Point"""
        self.sap = network.WLAN(network.AP_IF)
        self.sap.active(1)
        self.sap.config(essid=self.essid, password=self.password, security=self.security)
        while not self.sap.active():
            pass

    def _init_socket(self):
        """Init Socket"""
        self.skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.skt.bind(('', 80))
        self.skt.listen(MAX_CONNECTION)

    def _init_web_server(self):
        """Init Web Server"""
        _thread.start_new_thread(self._web_server, ())

    def _deinit_sap(self):
        """Deinit Soft Access Point"""
        self.sap.active(0)
        while self.sap.active():
            pass

    def _web_server(self):
        """Web Server"""
        while 1:
            conn, addr = self.skt.accept()
            if self.debug: print(f'Got a connection from {addr}')
            request = conn.recv(1024)
            if self.debug: print(request)
            response = self.http_msg
            conn.send(response)
            conn.close()
            sleep_ms(200)

    @property
    def http_msg(self):
        """Return HTTP message"""
        if isinstance(self._http_msg, (str, list, dict, tuple)):
            return self._http_msg
        return self._http_msg()

    @http_msg.setter
    def http_msg(self, http_msg):
        """Set HTTP message as a function or as a string, list, dict or tuple"""
        self._http_msg = http_msg
